#!/bin/bash

gchrome_x86=$(cat google-chrome.info | grep 'DOWNLOAD=' | sed 's/DOWNLOAD="//' | sed 's/"$//')
gchrome_amd64=$(cat google-chrome.info | grep 'DOWNLOAD_x86_64=' | sed 's/DOWNLOAD_x86_64="//' | sed 's/"$//')

echo 'download x86: '$gchrome_x86
echo 'download amd64: '$gchrome_amd64

if [ -z "$SKIP_DOWNLOAD" ]; then
	rm google-chrome-stable_current_i386.deb*
	rm google-chrome-stable_current_x86_64.deb*

	wget $gchrome_x86
	wget $gchrome_amd64
fi

md5_x86=$(md5sum google-chrome-stable_current_i386.deb | awk '{ print $1 }')
md5_x86_64=$(md5sum google-chrome-stable_current_amd64.deb | awk '{ print $1 }')

v_x86=$(ar p google-chrome-stable_current_i386.deb control.tar.gz 2> /dev/null | tar zxO ./control 2> /dev/null | grep Version | awk '{print $2}' | cut -d- -f1)
v_amd64=$(ar p google-chrome-stable_current_amd64.deb control.tar.gz 2> /dev/null | tar zxO ./control 2> /dev/null | grep Version | awk '{print $2}' | cut -d- -f1)
#v_x86=$(rpm -q -i -p google-chrome-stable_current_i386.deb | grep Version | awk '{ print $3 }')
#v_amd64=$(rpm -q -i -p google-chrome-stable_current_x86_64.deb | grep Version | awk '{ print $3 }')

cp google-chrome.info.dist google-chrome.info

perl -pi -e "s/{%version%}/${v_x86}/g" google-chrome.info
perl -pi -e "s/{%md5.x86%}/${md5_x86}/g" google-chrome.info
perl -pi -e "s/{%md5.x86_64%}/${md5_x86_64}/g" google-chrome.info
